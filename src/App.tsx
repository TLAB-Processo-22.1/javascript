import { Heading, Button, Stack, Flex, Text } from "@chakra-ui/react";

export function App() {

  function start(){
    console.log('Iniciou')
  }
  return (
    <>
    <Flex alignItems='center' justifyContent='center'>
    <Heading >Cronômetro</Heading>
    </Flex>

    <Flex alignItems='center' justifyContent='center' mt='3rem'>
      <Heading size='xl'>00:00</Heading>
    </Flex>

    <Flex alignItems='center' justifyContent='center' mt='1rem'>
      
      <Stack direction='row' spacing={4} align='center'>
        <Button 
          colorScheme='green'
          variant='solid'
          onClick={start}
           >
            Iniciar
        </Button>
        <Button colorScheme='gray' variant='solid'>
            Pausar
        </Button>
        <Button colorScheme='red' variant='solid'>
            Parar
        </Button>
       
      </Stack>
      </Flex>
    </>
  );
}

